% Author: Marcos M
% Date: 25/04/24

## Historia inicial
Se inicia en una agencia de exploración intergaláctica donde hay que elegir los personajes con 
los que se va a jugar (puede haber 2 personajes de cada para elegir)
En la agencia se pueden visitar varios sitios
- Despacho del director (donde se elegirá a los personajes)
- Zona de lanzamiento
- Zona de entrenamiento
El personaje (Que es un gran aventurero y explorador) se presenta voluntario para la misión y 
habla con el director de la agencia que le habla sobre una misión de exploración en la que falta 
un pasajero. La misión la componen un capitán, un segundo de abordo, un mecánico y un científico. 
Se da la opción de elegir entre 2 individuos por personaje.
igual son muchos personajes si damos la opcion de elegir para simplificar podrian ser solo 1 de 
cada y listo

## Capítulo 1: Preparación de la misión
El personaje principal, resulta que trabaja en la AEI (Agencia de Exploración Intergaláctica). Un 
dia cualquiera, mientras que se encuentra trabajando en su despacho de la AEI, cuando el jefe 
supremo le dice: "A partir de ahora, trabajarás en la nueva misión con objetivos de llegar a un 
planeta inexplorado, por lo que te encargarás de organizar, planificar y guiar la misión", "Esto 
significa que irás en el cohete". Este le contesta "Ohh, que gran oportunidad; claro que sí"
Desde este momento, el personaje principal con un grupo de ingenieros aeroespaciales a planificar 
la misión y a fabricar el cohete

[comment]("Aquí se podrá un puzzle o se completará la historia hasta el día de la misión")

## Capítulo 2: Día de la misión espacial
Después de haber preparado la misión espacial, se prepara el día de la misión; pero el personaje 
principal se queda dormido, así que se tiene que retrasar la misión dos días y el personaje 
principal tiene que pagar una multa. Después de estos dos días, sale la misión espacial, y se 
encuentran que saliendo la estratosfera, el cohete muestra un error, que dice que hay problemas 
con la presurización, así que aterrizan para reparar el cohete.
Por tercera vez, llega el día de la misión espacial, y esta vez todo sale correctamente. El 
cohete, hace una parada en la ISS (Estación Espacial Internacional). Aquí recargan combustible 
para el cohete y hacen una cambio de pasajeros.