% Sofía
% 1/5/24

## Descripción del juego

Aventura gráfica con puzzles y alguna que otra pelea.

## Puzzles

Sección con todos los tipos de puzzles que habrá en el juego.

### Preguntas y respuestas

#### Menús

Funciona a través de menús. Al jugador se le presenta una pregunta y éste debe
responderla eligiendo una de las opciones del menú. Al responder la pregunta
incorrectamente, se añade uno a una especie de contador. Al llegar a 3 fallos,
el jugador se queda sin intentos y debe empezar de nuevo.

#### Entrada de texto

Otro tipo de preguntas a las cuáles el jugador debe responer con entradas de
texto. La respuesta debe estar escrita sin ningún fallo o falta de ortografía,
que renpy no llega pa tanto. Por lo demás es igual a las preguntas con menús.

### Encontrar objetos

Usando imagebuttons, el jugador debe encontrar un determinado número de objetos,
que podrían ser cualquier cosa, que se encuentran escondidos en un escenario.
Al encontar todos los objetos, el jugador puede continuar con la historia.
Opciones para aumentar la dificultad:
- Impostores: Se pueden añadir objetos que el jugador no está buscando, y por lo
tanto, al hacer click en ellos, se añade uno al contador de fallos, como en las
preguntas y respuestas. Al llegar a 3 fallos, comienza de nuevo.
- Cronómetro: El jugador tiene un límite de tiempo para encontrar los objetos.
Si no lo logra, empieza de nuevo. Al llegar a cierto número de intentos fallidos
se añade una opción de aumentar el tiempo del que el jugador dispone para
encontrar todos los objetos.

Seguramente queda algún que otro tipo de puzzle, pero cuando escribí esto no me
acordé.

## Batallas

Aquí llega lo interesante :)

Las típicas batallas por turnos. Las estadísticas de los personajes se eligen al
principio de la partida.

### Características de los personajes

#### Fuerza

Daño de ataque básico.

#### Resistencia

Vida máxima.

#### Velocidad

Cuanto más alta, más probabilidades de ganar 1 turno extra.

#### Inteligencia

Cuanto más alta, más probabilidades de predecir el movimiento de tu oponente.

#### Evasión

Cuanto más alta, más probabilidades de esquivar el ataque de tu oponente.

#### Precisión

Cuanto más alta, menos probabilidades de fallar un movimiento.

### Mecánicas de combate

#### Recargas

Antes de nada, he pensado en usar este sistema que utilizan muchos juegos de
este tipo para mantener las cosas equilibradas. Se trata de que, cada turno
que se usa un ataque básico, se gana una "recarga". Se pueden acumular hasta 3,
pero ese número se puede aumentar a medida que progresa la historia y las
batallas se complican. Las recargas sirven para usar un ataque más potente, o
aumentar una de las características del personaje. Todas las batallas se
empiezan con al menos una recarga.

#### Ataque básico

El personaje sencillo que no hace demasiado daño. Se gana una recarga.

#### Movimiento recargado

Usando recargas, puedes elegir a través de un menú si vas a aumentar una de las
características del personaje (fuerza, resistencia, etc.) o usar un ataque más
potente que el básico.

#### Ataque definitivo

Un ataque muy potente que solo se puede usar al alcanzar cierto requisito que
aún tengo que pensar, pero se me ocurre que podría ser que cada turno se añade
un 15 a una variable que al llegar a 100 o más te permite usar este ataque.

### Ejemplo batalla

#### Estadísticas personajes ejemplo:

Protagonista:

- Fuerza: 25
- Resistencia: 120
- Velocidad: 40
- Inteligencia: 45
- Evasión: 23
- Precisión: 89

Enemigo:

- Fuerza: 12
- Resistencia: 50
- Velocidad: 21
- Inteligencia: 1
- Evasión: 45
- Precisión: 78

#### Turno 1

Usas el ataque básico. La resistencia del enemigo pasa a ser 25. Has tenido
suerte, tu estadística de velocidad a servido para garantizarte un turno extra.
Tienes ahora 2 recargas

#### Turno 2

Decides usar una recarga para mejorar tus habilidades. Ahora tienes un 50 de
evasión. Te queda una recarga

#### Turno enemigo 1

Ha usado un ataque básico. Tu resistencia es ahora de 108, ya que no pudiste
esquivar el ataque con ese nivel de evasión, perdiste el 50/50.

#### Turno 3

Usas un ataque básico. Has logrado derrotar al enemigo.
