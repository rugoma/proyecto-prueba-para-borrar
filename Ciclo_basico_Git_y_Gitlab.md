% Ciclo básico de uso de Git y Gitlab
% Grupo de robótica del curso 2023/24
% 25 de abril de 2024

## Introducción

En este archivo vamos a resumir los pasos básicos para usar Git y Gitlab.

## Creación de cuenta en Gitlab

## Creación de nuevo repositorio en Gitlab

## Replicación (o clonación) en local

Los pasos necesarios son:

1. Ir a la carpeta donde queremos copiar el repositorio
2. Ejecutar `git clone dirección_del_repositorio`
3. Añadir ficheros
4. Ir memorizando (*«commits»*).

Esos pasos con comandos son los siguientes:

2. git clone git@gitlab.com:rugoma/proyecto-prueba-para-borrar.git
3. Crear un fichero nuevo
4. git add Ciclo_basico_Git_y_Gitlab.md
5. git commit -am "Memorización inicial: archivo Markdown en blanco."
6. Empiezo a editar el fichero.

### Sincronización con Gitlab

Si estoy trabajando solo, los primeros pasos ya los hemos hecho, entonces
el siguiente paso sería subir los cambios que he hecho a Gitlab:

```
git push
```

*«Push»* significar empujar en inglés, así que una traducción literal sería
«empujar mis cambios a Gitlab».

Imaginemos que estoy trabajando con más gente, el repositorio ya está más
avanzado y se que otros compañeros ya han hecho cambios.

**Antes de empezar a trabajar** debo sincronizar mi repositorio:

```
git pull
```

*«Pull»* significa tirar en inglés, por lo tanto, la traducción literal sería
«tirar de los cambios» (hacia mi, hacia mi ordenador).

A partir de ahí, ya haría mi trabajo (editar ficheros, crear nuevos, etc.) e
iría haciendo mis memorizaciones.

En el momento en que termino, hago `git push`.

## Trabajo con ramas

Vamos a seguir el flujo característico de Git, conocido como *Git flow*.

Para ver cuantas ramas tiene el proyecto, utilizamos:

```
git branch
```

Digamos que queremos crear una nueva rama:

```
git checkout -b nombre_rama
```
Hacemos nuestro ciclo de trabajo habitual, con sus
memorizaciones, etc., etc.

Cuando terminamos el trabajo en la rama y comprobamos que
todo está bien, tenemos que cambiar a la rama principal y
fusionar los cambios:

```
git checkout main
git merge nombre_rama
```
